import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import Swal from 'sweetalert2';


@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  private URL_SERVER = "https://restcountries.com/v3.1";

  constructor( private http: HttpClient ) { }

  getCountries() {
    const url = `${ this.URL_SERVER }/all`;
    return this.http.get( url ) .pipe(
      catchError( err => {
        const alert = {
          icon: 'error',
          title: 'Error al cargar el pais, intenta de nuevo.'
        };
        this.showAlert( alert );
        return throwError( err );
      })
    );
  }

  searchCountry(country:string){
    const url = `${ this.URL_SERVER }/name/${country}`;
    return this.http.get( url ) .pipe(
      catchError( err => {
        const alert = {
          icon: 'error',
          title: 'Error al cargar el listado de paises, intenta de nuevo.'
        };
        this.showAlert( alert );
        return throwError( err );
      })
    );
  }

  showAlert( alert: any ) {

    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      onOpen: (toast) => {
        toast.addEventListener( 'mouseenter', Swal.stopTimer )
        toast.addEventListener( 'mouseleave', Swal.resumeTimer )
      }
    });

    Toast.fire( alert );
  }
}
