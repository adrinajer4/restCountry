/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { List_countriesComponent } from './list_countries.component';

describe('List_countriesComponent', () => {
  let component: List_countriesComponent;
  let fixture: ComponentFixture<List_countriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ List_countriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(List_countriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
