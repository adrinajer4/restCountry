import { Component, OnInit } from '@angular/core';
import { CountriesService } from '../../services/countries.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-list_countries',
  templateUrl: './list_countries.component.html',
  styleUrls: ['./list_countries.component.css']
})
export class List_countriesComponent implements OnInit {
  public countries: any[] = [];
  btnOk: boolean = false;

  constructor(
    private router: Router,
    private countriesService: CountriesService
  ) { }

  ngOnInit() {
    this.cargarCountries();
  }
  onSearchChange(): void {
    var x = (<HTMLInputElement>document.getElementById("form1")).value;
    if(x){
      this.btnOk=true;
    }else{
      this.btnOk=false;
      this.cargarCountries();
    }
  }

  searchCountry() {
    console.log("buscando");
    let nombre = (<HTMLInputElement>document.getElementById("form1")).value;
    this.countriesService.searchCountry(nombre).subscribe((countries: any) => {
      this.countries = countries;
    });
  }

  cargarCountries() {
    this.countriesService.getCountries().subscribe((countries: any) => {
      this.countries = countries;
    });
  }

}
